package com.example.user.myibeacon;

/**
 * Created by user on 2017/3/13.
 */

public class mydeviceslist {
    /*
    E7:C0:D2:0A:96:E0->move_2
    EC:83:F4:0A:0E:33->12345678
    CE:A3:53:FC:08:E2->A3
     */
    public static String device_address[] = {
//            "E7:C0:D2:0A:96:E0",
//            "EC:83:F4:0A:0E:33",
//            "CE:A3:53:FC:08:E2",
            "D3:4C:C6:38:06:F0",//1
            "D0:84:F8:1E:F4:D4",//2
            "C0:84:26:56:F9:5E",//3
            "E6:26:96:CE:DC:7F",//4
            "CB:F5:50:9F:72:BE",//5
            "D7:7C:C2:9A:9B:80",//6
            "D2:E2:43:73:F0:B2",//7
            "DC:A7:BE:5B:DE:FF",//8
            "D1:DD:43:1B:53:8F",//9
    };
    public static String device_position[][] = {
            {"10", "10"},//1
            {"18.8", "9.5"},//2
            {"10", "19.5"},//3
            {"18.8", "19.5"},//4
            {"11", "0.5"},//5
            {"18.8", "6"},//6
            {"1.9", "2"},//7
            {"2.0", "10"},//8
            {"2.5", "19.5"},//9
    };
}
