package com.example.user.myibeacon;

import android.Manifest;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.nordicsemi.android.support.v18.scanner.ScanResult;

public class MainActivity extends AppCompatActivity implements mycallback, View.OnClickListener {
    private ListView lv_devices;
    private Button btn_start;
    private TextView txt_output;
    private TextView txt_debug;
    private MyAdapter adapter;
    private BleService myble;
    private List<BluetoothDevice> ble_devices = new ArrayList<>();
    private List<Double> devices_rssi = new ArrayList<>();
    private List<String> devicds_list = new ArrayList<>();
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private List<ScanResult> results = new ArrayList<>();
    private Handler handler_adddevice = new Handler();
    private Handler handler_calcpostion = new Handler();
    private Runnable runnable_adddevice = new Runnable() {
        @Override
        public void run() {
            List<BluetoothDevice> chkdevices = new ArrayList<>();
            for (ScanResult result : results) {
                BluetoothDevice device = result.getDevice();
                double rssi = Math.abs(result.getRssi());
                rssi = calc_distance(rssi);
                if (devicds_list.contains(device.getAddress())) {
                    if (!chkdevices.contains(device)) {
                        chkdevices.add(device);
                    }
                    if (ble_devices.contains(device)) {
                        devices_rssi.set(ble_devices.indexOf(device), rssi);
                    } else {
                        ble_devices.add(device);
                        devices_rssi.add(rssi);
                    }
                }
            }
//            Log.d("show_chk_count", String.valueOf(chkdevices.size()));
            while ((chkdevices.size() != ble_devices.size())) {
                for (BluetoothDevice device : ble_devices) {
                    if (!chkdevices.contains(device)) {
                        Log.d("delete_device", "run");
                        int n = ble_devices.indexOf(device);
                        ble_devices.remove(n);
                        devices_rssi.remove(n);
                        break;
                    }
                }
            }
            if (ble_devices.size() >= 3) {
                handler_calcpostion.post(runnable_calcpostion);
            }
            adapter.notifyDataSetChanged();
        }
    };
    private Runnable runnable_calcpostion = new Runnable() {
        @Override
        public void run() {
            List<String> devices = new ArrayList<>();
            String position[][];
            devices.addAll(Arrays.asList(mydeviceslist.device_address));
            position = mydeviceslist.device_position;
            double x[] = new double[3];
            double y[] = new double[3];
            double r[] = new double[3];
            Map<Double, BluetoothDevice> mymap = new HashMap<>();
            for (int i = 0; i < ble_devices.size(); i++) {
                mymap.put(devices_rssi.get(i), ble_devices.get(i));
            }
            Collections.sort(devices_rssi);
            for (int i = 0; i < 3; i++) {
                int n = devices.indexOf(mymap.get(devices_rssi.get(i)).getAddress());
                x[i] = Double.parseDouble(position[n][0]);
                y[i] = Double.parseDouble(position[n][1]);
                r[i] = devices_rssi.get(i);
                Log.d("xyr", String.valueOf(x[i]) + "==" + String.valueOf(y[i]) + "==" + String.valueOf(r[i]) + "==");
            }
            Ponto anspos = mytrilateration.getLocationByTrilateration(
                    new Ponto(x[0], y[0]), r[0],
                    new Ponto(x[1], y[1]), r[1],
                    new Ponto(x[2], y[2]), r[2]
            );
            txt_debug.setText(String.format("x1:%f y1:%f r1:%f\nx2:%f y2:%f r2:%f\nx3:%f y3:%f r3:%f\n",
                    x[0],y[0],r[0],
                    x[1],y[1],r[1],
                    x[2],y[2],r[2]));
            txt_output.setText(anspos.emPalavras());
//            double anspos[] = mytrilateration.calcposition(x[0], y[0], x[1], y[1], x[2], y[2], r[0], r[1], r[2]);
//            Log.d("anspos", String.valueOf(anspos[0]) + "," + String.valueOf(anspos[1]));
//            txt_output.setText(String.valueOf(anspos[0]) + "," + String.valueOf(anspos[1]));

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermission();
        myble = new BleService(this, this);
        devicds_list.addAll(Arrays.asList(mydeviceslist.device_address));
        init_view();
    }

    private void init_view() {
        lv_devices = (ListView) findViewById(R.id.lv_devices);
        adapter = new MyAdapter();
        lv_devices.setAdapter(adapter);
        txt_output = (TextView) findViewById(R.id.txt_output);
        txt_debug = (TextView)findViewById(R.id.txt_debug);
        btn_start = (Button) findViewById(R.id.btn_start);
        btn_start.setOnClickListener(this);
    }

    //        private double calc_distance(double rssi) {
//        double txPower = 77;
//        double ratio = rssi * 1.0 / txPower;
//        if (ratio < 1.0) {
//            return Math.pow(ratio, 10);
//        } else {
//            double accuracy = (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
//            return accuracy;
//        }
//    }
    private double calc_distance(double rssi) {
        double txPower = 64;
        double ratio_db = rssi - txPower;
        double ratio_linear = Math.pow(10, ratio_db / 10);
        double r = Math.sqrt(ratio_linear);
        return r;
    }

    @Override
    public void setdevices(final List<ScanResult> results) {
        //Log.d("setdevices", "setdevices");
        this.results = results;
        for(ScanResult item : results){
            Log.d("device_address",item.getDevice().getAddress());
        }
        handler_adddevice.post(runnable_adddevice);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start:
                myble.startscan();
                break;
            default:
                break;
        }
    }

    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return ble_devices.size();
        }

        @Override
        public Object getItem(int i) {
            return ble_devices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup parent) {

            LayoutInflater inflater = getLayoutInflater();
            View row;
            row = inflater.inflate(R.layout.ble_devices, parent, false);
            TextView device_name = (TextView) row.findViewById(R.id.device_name);
            TextView device_rssi = (TextView) row.findViewById(R.id.device_rssi);
            device_name.setText(ble_devices.get(i).getName() + ble_devices.get(i).getAddress());
            device_rssi.setText(String.valueOf(devices_rssi.get(i)));
            return row;
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check 
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This app needs location access");
                builder.setMessage("Please grant location access so this app can detect beacons.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {


                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }

                });
                builder.show();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("permission", "coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }

}
