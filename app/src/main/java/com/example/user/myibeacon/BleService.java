package com.example.user.myibeacon;

import android.Manifest;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Button;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;

/**
 * Created by user on 2016/11/26.
 */

public class BleService {

    private BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();
    private List<BluetoothDevice> my_device_list = new ArrayList<BluetoothDevice>();
    private ScanCallback callback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            //myblecallback.setdevices(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
//            Log.d("batch_show", "test");
            int i = 1;
            for (ScanResult device : results) {
                //Log.d("device" + String.valueOf(i), device.getDevice().getName());
                i++;
            }
            myblecallback.setdevices(results);

        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
        }
    };
    private BluetoothDevice selected_device = null;
    private BluetoothGatt mGatt = null;
    private BluetoothGattCharacteristic myCharacter = null;
    private BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                gatt.discoverServices();
                Log.i("connect", "success");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            List<BluetoothGattService> services = gatt.getServices();
            Log.d("services", "disconver");
            if (status == BluetoothGatt.GATT_SUCCESS) {
                myCharacter = services.get(2).getCharacteristics().get(0);
                gatt.readCharacteristic(services.get(2).getCharacteristics().get(0));
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d("characteristic", "reading");
                //broadcastUpdate(characteristic);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            broadcastUpdate(characteristic);
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
        }
    };
    private final static UUID CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private Context context;
    private String output = "";
    private List<byte[]> store_data = new ArrayList<>();
    private boolean first = true;
    private mycallback myblecallback;

    protected BleService(Context context, mycallback callback) {
        this.context = context.getApplicationContext();
        this.myblecallback = callback;
    }

    protected void startscan() {
        Log.d("scan", "start");
        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(3000).build();
        List<ScanFilter> filters = new ArrayList<ScanFilter>();
        scanner.startScan(filters, settings, callback);

    }

    protected void ble_shutdown() {
        if (mGatt != null) {
            Log.d("ble", "shutdown success");
            mGatt.disconnect();
            mGatt.close();

        }
    }

    protected List<BluetoothDevice> get_list() {
        return my_device_list;
    }

    protected void select_device(int index) {
        selected_device = my_device_list.get(index);
        Log.i("select device", (selected_device.getName() == null ? "null" : selected_device.getName()));
    }

    protected String get_selected_device_name() {
        return selected_device.getName();
    }

    protected String get_selected_device_address() {
        return selected_device.getAddress();
    }

    protected void connect_device() {
        mGatt = selected_device.connectGatt(context, true, gattCallback);
    }

    protected int connection_status() {
        BluetoothManager bleManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        return bleManager.getConnectionState(selected_device, BluetoothProfile.GATT);
        //return mGatt.getConnectionState(selected_device);
    }

    protected void enableNotify() {
        enableNotifications(myCharacter);
    }

    private final boolean enableNotifications(final BluetoothGattCharacteristic characteristic) {
        final BluetoothGatt gatt = mGatt;
        if (gatt == null || characteristic == null)
            return false;

        // Check characteristic property
        final int properties = characteristic.getProperties();
        if ((properties & BluetoothGattCharacteristic.PROPERTY_NOTIFY) == 0)
            return false;

        Log.d("BLE", "gatt.setCharacteristicNotification(" + characteristic.getUuid() + ", true)");
        gatt.setCharacteristicNotification(characteristic, true);
        final BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR_UUID);
        if (descriptor != null) {
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            Log.v("BLE", "Enabling notifications for " + characteristic.getUuid());
            Log.d("BLE", "gatt.writeDescriptor(" + CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR_UUID + ", value=0x01-00)");
            return gatt.writeDescriptor(descriptor);
        }
        return false;
    }

    private void broadcastUpdate(final BluetoothGattCharacteristic characteristic) {
        byte[] temp = characteristic.getValue();
        String tempstr = "";
        store_data.add(temp);
        for (int i = 0; i < temp.length; i++) {
            if (temp[i] == 3) {
                tempstr += "口";
                break;
            } else {
                tempstr += Character.toString((char) temp[i]);
            }
        }
        Log.i("show_pre", tempstr);

    }

    protected void tran_data() {
        output = "";
        for (int i = 0; i < store_data.size(); i += 1) {
            byte[] temp = store_data.get(i);
            for (int j = 0; j < temp.length; j++) {
                if (temp[j] == 3) {
                    if (output.length() > 70) {
                        if (first) {
                            first = false;
                        } else {
                            show_data();
                        }
                    }
                    output = "";
                    break;
                } else {
                    output += Character.toString((char) temp[j]);
                }
            }
        }
    }

    private void show_data() {
//        Log.i("connect", output);
//        DBControl db = new DBControl(context);
//        db.dbinsert("BleDataTest", output);
    }

}
