package com.example.user.myibeacon;

import android.os.Handler;

/**
 * Created by user on 2017/1/18.
 */

public class mytimer {
    private int count;
    private mycallback callback;
    public mytimer(mycallback callback){
        this.callback = callback;
        count = 0;
        start();
    }
    public void start() {
        mHandlerTime.postDelayed(timerRun, 1000);
    }

    private Handler mHandlerTime = new Handler();
    private final Runnable timerRun = new Runnable() {
        public void run() {
            //callback.settext(String.valueOf(count));
            count++;
            mHandlerTime.postDelayed(this, 1000);
        }
    };
}
