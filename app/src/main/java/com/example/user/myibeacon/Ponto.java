package com.example.user.myibeacon;

/**
 * Created by user on 2017/3/21.
 */

public class Ponto {
    protected double x, y;

    public Ponto() {
        setPonto(0, 0);
    }

    public Ponto(double a, double b) {
        setPonto(a, b);
    }

    public void setPonto(double a, double b) {
        x = a;
        y = b;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String emPalavras() {
        return "[" + x + ", " + y + "]";
    }
}
